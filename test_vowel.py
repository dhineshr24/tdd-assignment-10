import pytest
from countVowel import Vowels
from unittest.mock import MagicMock


class TestClass:

    @classmethod
    def setup_class(cls):
        print("\nSetting up module")

    @classmethod
    def teardown_class(cls):
        print("\nTearing down module")

    @pytest.fixture()
    def vowels(self):
        vowels = Vowels()
        return vowels

    def test_count_vowels_1(self, vowels):
        demo = "demo"
        vowels.add_string(demo)
        assert vowels.count_vowels() == 2

    @pytest.mark.parametrize("string, result", [("dscs", 0), ("absaa", 3)])
    def test_count_multiple_vowels(self, vowels, string, result):
        vowels.add_string(string)
        assert vowels.count_vowels() == result

    def test_count_vowels_exception(self, vowels):
        demo = 2
        with pytest.raises(Exception):
            vowels.add_string(demo)

    def test_return(self, vowels, monkeypatch):
        mock_file = MagicMock()
        mock_file.readline = MagicMock(return_value="abcde")
        mock_open = MagicMock(return_value=mock_file)
        monkeypatch.setattr("builtins.open", mock_open)
        result = vowels.readFromFile("input_hu")
        mock_open.assert_called_once_with("input_hu", "r")
        vowels.add_string(result)
        assert 2 == vowels.count_vowels()




