

class Vowels:
    vowels = ['a', 'e', 'i', 'o', 'u']
    str = ""

    def __init__(self):
        self.str = ""

    def add_string(self, s):
        if type(s) != str:
            raise Exception("String inputs only")
        self.str = s

    def count_vowels(self):
        count = 0
        for i in self.str:
            if i in self.vowels:
                count += 1
        return count

    def readFromFile(self, filename):
        infile = open(filename, "r")
        line = infile.readline()
        return line


obj = Vowels()
obj.add_string("demo")
print(obj.count_vowels())
